﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Flag
{
    public static class FlagListenerExtensions
    {
        public static void UseFlagListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IFlagListener>().Start();
        }
    }
}