﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.DataAttributes;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;

namespace LendFoundry.Flag {
    public class FlagService : IFlagServiceExtended {
        public FlagService
            (
                ILogger logger,
                IEventHubClient eventhub,
                ILookupService lookupService,
                IDataAttributesEngine dataAttributeEngine,
                IFlagRepository flagRepository,
                ITokenHandler tokenParser,
                ITokenReader tokenReader
            ) {
                if (flagRepository == null)
                    throw new ArgumentException ($"{nameof(FlagRepository)} is mandatory");

                if (logger == null)
                    throw new ArgumentException ($"{nameof(logger)} is mandatory");

                if (eventhub == null)
                    throw new ArgumentException ($"{nameof(eventhub)} is mandatory");

                if (lookupService == null)
                    throw new ArgumentException ($"{nameof(lookupService)} is mandatory");

                if (tokenParser == null)
                    throw new ArgumentException ($"{nameof(tokenParser)} is mandatory");

                if (tokenReader == null)
                    throw new ArgumentException ($"{nameof(tokenReader)} is mandatory");

                Logger = logger;
                Eventhub = eventhub;
                LookupService = lookupService;
                DataAttributeEngine = dataAttributeEngine;
                FlagRepository = flagRepository;
                TokenParser = tokenParser;
                TokenReader = tokenReader;
            }

        private IDataAttributesEngine DataAttributeEngine { get; }

        private ILogger Logger { get; }

        private IEventHubClient Eventhub { get; }

        private ILookupService LookupService { get; }

        private IFlagRepository FlagRepository { get; }
        private ITokenHandler TokenParser { get; }
        private ITokenReader TokenReader { get; }

        public IEnumerable<IFlag> GetAllFlag (string entityType, string entityId) {
            entityType = Validate (entityType, entityId);

            return FlagRepository.GetAllFlags (entityType, entityId);
        }

        public async Task<IFlag> AddFlag (string entityType, string entityId, IFlagRequest request) {
            entityType = Validate (entityType, entityId);

            if (request == null)
                throw new InvalidArgumentException ($"#{nameof(request)} cannot be null", nameof (entityId));

            var username = GetUserName ();
            var productId = await GetProductId (entityType, entityId);
            var flag = new Flag {
                EntityId = entityId,
                EntityType = entityType,
                FlagTags = request.FlagTags,
                Action = request.Action,
                FlagDetail = request.FlagDetail,
                Description = request.Description,
                FlagCreatedDate = DateTime.Now,
                EventData = request.EventData,
                Status = FlagStatus.Active,
                EventName = string.IsNullOrEmpty(request.EventName) ? "Manual" :request.EventName,
                UserName = username,
                ProductId = productId,
            };
            FlagRepository.Add (flag);
            var flags = GetAllFlag (entityType, entityId);
            await Eventhub.Publish (Settings.FlagAddedEventName, new { EntityId = entityId, EntityType = entityType, Flag = flag, ActiveFlags = flags.Where (x => x.Status == FlagStatus.Active).ToList ().Count });

            return flag;
        }

        public async Task<IFlag> UpdateStatus (string flagId, FlagStatus status) {
            if (string.IsNullOrWhiteSpace (flagId))
                throw new InvalidArgumentException ($"#{nameof(flagId)} cannot be null", nameof (flagId));

            var flagDetail = await FlagRepository.Get (flagId);
            if (flagDetail == null)
                throw new NotFoundException ($"Flag details not found for the FlagId {flagId}");

            flagDetail.Status = status;
            FlagRepository.Update (flagDetail);
            var flags = GetAllFlag (flagDetail.EntityType, flagDetail.EntityId);

            if (!flags.ToList ().Any (x => x.Status == FlagStatus.Active))
                await Eventhub.Publish ("FlagInActive", flagDetail);

            //for active flag count
            await Eventhub.Publish ("FlagStatusChange", new { EntityId = flagDetail.EntityId, EntityType = flagDetail.EntityType, Flag = flagDetail, ActiveFlags = flags.Where (x => x.Status == FlagStatus.Active).ToList ().Count });
            return flagDetail;

        }
        public void AddOrRemoveTags (string flagId, FlagActionRequest tagRequest) {
            if (string.IsNullOrWhiteSpace (flagId))
                throw new InvalidArgumentException ($"#{nameof(flagId)} cannot be null", nameof (flagId));

            if (tagRequest.FlagItems == null)
                throw new InvalidArgumentException ($"#{nameof(tagRequest.FlagItems)} cannot be null", nameof (tagRequest.FlagItems));

            if (tagRequest.Action == TagAction.Add) {
                tagRequest.FlagItems.ToList ().ForEach (x => {
                    if (!string.IsNullOrEmpty (x)) {
                        FlagRepository.AddTag (flagId, x);
                    }
                });
            } else {
                tagRequest.FlagItems.ToList ().ForEach (x => {
                    if (!string.IsNullOrEmpty (x)) {
                        FlagRepository.RemoveTag (flagId, x);
                    }
                });
            }
        }
        public void AddOrRemoveActions (string flagId, FlagActionRequest actionRequest) {
            if (string.IsNullOrWhiteSpace (flagId))
                throw new InvalidArgumentException ($"#{nameof(flagId)} cannot be null", nameof (flagId));

            if (actionRequest == null)
                throw new InvalidArgumentException ($"#{nameof(actionRequest)} cannot be null", nameof (actionRequest));

            if (actionRequest.FlagItems == null)
                throw new InvalidArgumentException ($"#{nameof(actionRequest.FlagItems)} cannot be null", nameof (actionRequest.FlagItems));

            if (actionRequest.Action == TagAction.Add) {
                actionRequest.FlagItems.ToList ().ForEach (x => {
                    if (!string.IsNullOrEmpty (x)) {
                        FlagRepository.AddAction (flagId, x);
                    }
                });
            } else {
                actionRequest.FlagItems.ToList ().ForEach (x => {
                    if (!string.IsNullOrEmpty (x)) {
                        FlagRepository.RemoveAction (flagId, x);
                    }
                });
            }
        }

        public Action<EventInfo> ProcessEvent (FlagConfiguration flagConfiguration, Dictionary<string, string> lookupEntries) {
            return @event => {
                try {
                    if (flagConfiguration == null)
                        throw new ArgumentException ($"The configuration for service #{Settings.ServiceName} could not be found, please verify");

                    if (flagConfiguration.Entities == null || !flagConfiguration.Entities.Any ())
                        throw new ArgumentException ($"The configuration related to Entities could not be found, please verify");

                    var eventData = @event.Data;
                    var data = JObject.FromObject (@event.Data).ToObject<JObject> ();
                    var configurations = new Dictionary<string, IEnumerable<FlagEventConfiguration>> ();
                    flagConfiguration.Entities
                        .Where (entity => entity.Value.Any (ev => ev.EventName.Equals (@event.Name, StringComparison.OrdinalIgnoreCase)))
                        .Select (e => {
                            configurations.Add (e.Key, e.Value.Where (ev =>
                                ev.EventName.Equals (@event.Name, StringComparison.OrdinalIgnoreCase)).ToList ());
                            return e;
                        }).ToList ();

                    foreach (var keyValue in configurations) {
                        var entityType = ValidateEntityType (keyValue.Key, lookupEntries);

                        keyValue.Value.ToList ().ForEach (configuration => {
                            var entityId = Convert.ToString (InterpolateExtentions.GetPropertyValue (configuration.EntityId.Replace ("{", "").Replace ("}", ""), @event));

                            var falgEvents = flagConfiguration.Entities
                                .FirstOrDefault (entity => entity.Key.Equals (entityType, StringComparison.OrdinalIgnoreCase) &&
                                    entity.Value.Any (ev => ev.EventName.Equals (@event.Name, StringComparison.OrdinalIgnoreCase)));
                            var falgToBeCreate = falgEvents.Value.FirstOrDefault (ev => ev.EventName.Equals (@event.Name, StringComparison.OrdinalIgnoreCase));
                            if (falgToBeCreate == null)
                                throw new ArgumentException ($"The flag configuration could not found for event name #{@event.Name} , please verify");

                            Task.Run (() => InsertFlag (@event, entityType, entityId, @event.Name, eventData, falgToBeCreate));

                            Logger.Info ($"The EntityType: #{entityType}, EntityId: #{entityId}, with event #{configuration.EventName} was processed");

                            Logger.Info ($"Event #{Settings.FlagAddedEventName} for #{entityType} with id #{entityId} was published\n");
                        });
                    }
                } catch (Exception ex) {
                    Logger.Error ($"Error while processing event #{@event.Name}. Error: {ex.Message}", ex);
                    Logger.Info ("\nFlag is working yet and waiting new event\n");
                }
            };
        }

        private async Task InsertFlag (EventInfo @event, string entityType, string entityId, string eventName, object eventData, FlagEventConfiguration flagEvent) {
            @event.Data = JObject.FromObject (@event.Data);
            if (flagEvent == null)
                throw new ArgumentException ($"{nameof(flagEvent)} cannot be null");

            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentException ("An activity must to have a EntityId");

            if (string.IsNullOrWhiteSpace (entityType))
                throw new ArgumentException ("An activity must to have a EntityType");

            var productId = await GetProductId (entityType, entityId);
            var username = GetUserName ();
            var flag = new Flag {
                EntityId = entityId,
                EntityType = entityType,
                ProductId = productId,
                FlagTags = flagEvent.Tags ?? new string[0],
                Action = flagEvent.Action,
                FlagDetail = flagEvent.FlagDetail,
                Description = flagEvent.Description.FormatWith (@event),
                FlagCreatedDate = DateTime.Now,
                EventData = eventData,
                Status = FlagStatus.Active,
                EventName = eventName,
                UserName = username
            };

            FlagRepository.Add (flag);
            var flags = GetAllFlag (entityType, entityId);
            await Eventhub.Publish (Settings.FlagAddedEventName, new { EntityId = entityId, EntityType = entityType, Flag = flag, ActiveFlags = flags.Where (x => x.Status == FlagStatus.Active).ToList ().Count });

            Logger.Info ($"The Flag Details for EntityType: #{flag.EntityType}, EntityId: #{flag.EntityId} added in database");
        }
        private string Validate (string entityType, string entityId) {
            if (string.IsNullOrWhiteSpace (entityType))
                throw new InvalidArgumentException ($"#{nameof(entityType)} cannot be null", nameof (entityType));

            if (string.IsNullOrWhiteSpace (entityId))
                throw new InvalidArgumentException ($"#{nameof(entityId)} cannot be null", nameof (entityId));

            var lookupEntries = LookupService.GetLookupEntries ("entityTypes");

            return ValidateEntityType (entityType, lookupEntries);
        }
        public async Task<string> GetProductId (string entityType, string entityId) {
            string productId = string.Empty;
            var applicationAttributes = await DataAttributeEngine.GetAttribute (entityType, entityId, "product");
            if (applicationAttributes != null)
                productId = GetResultValueForProductId (applicationAttributes);

            return productId;
        }

        public IEnumerable<IFlag> GetAllFlagsByEventName (string entityType, string eventName) {
            if (string.IsNullOrWhiteSpace (entityType)) {
                throw new InvalidArgumentException ($"#{nameof(entityType)} cannot be null", nameof (entityType));
            }

            if (string.IsNullOrWhiteSpace (eventName)) {
                throw new InvalidArgumentException ($"#{nameof(eventName)} cannot be null", nameof (eventName));
            }

            return FlagRepository.GetAllFlagsByEventName (entityType, eventName);
        }

        public IEnumerable<IFlag> GetAllFlagsByEntityType (string entityType) {

            if (string.IsNullOrWhiteSpace (entityType))
                throw new InvalidArgumentException ($"#{nameof(entityType)} cannot be null", nameof (entityType));

            return FlagRepository.GetAllFlagsByEntityType (entityType);
        }

        public IEnumerable<IFlag> GetAllFlagsByTag (string entityType, string tagName) {

            if (string.IsNullOrWhiteSpace (entityType))
                throw new InvalidArgumentException ($"#{nameof(entityType)} cannot be null", nameof (entityType));

            if (string.IsNullOrWhiteSpace (tagName))
                throw new InvalidArgumentException ($"#{nameof(tagName)} cannot be null", nameof (tagName));

            return FlagRepository.GetAllFlagsByTag (entityType, tagName);
        }

        private static string GetResultValueForProductId (dynamic data) {
            Func<dynamic> resultProperty = () => data[0].ProductId;
            return HasProperty (resultProperty) ? GetValue (resultProperty) : string.Empty;
        }

        private static bool HasProperty<T> (Func<T> property) {
            try {
                property ();
                return true;
            } catch (RuntimeBinderException) {
                return false;
            }
        }

        private static T GetValue<T> (Func<T> property) {
            return property ();
        }

        private string ValidateEntityType (string entityType, Dictionary<string, string> lookups) {
            entityType = entityType.ToLower ();
            if (!lookups.ContainsKey (entityType))
                throw new InvalidArgumentException ($"Invalid entityType {entityType}", nameof (entityType));
            return entityType;
        }

        private string GetUserName () {
            var token = TokenParser.Parse (TokenReader.Read ());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace (token?.Subject))
                throw new ArgumentException ("User is not authorized");
            return username;
        }

    }
}