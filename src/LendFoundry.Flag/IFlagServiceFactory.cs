﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Flag
{
    public interface IFlagServiceFactory
    {
        IFlagServiceExtended Create(StaticTokenReader reader, ILogger logger,ITokenHandler tokenHandler);
    }
}