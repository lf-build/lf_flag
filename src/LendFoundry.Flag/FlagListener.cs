﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;
using System.Net.WebSockets;

namespace LendFoundry.Flag
{
    public class FlagListener : IFlagListener
    {
        public FlagListener
        (
            IFlagServiceFactory activityLogServiceFactory,
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ILookupClientFactory lookupServiceFactory
        )
        {
            if (activityLogServiceFactory == null)
                throw new ArgumentException($"{nameof(activityLogServiceFactory)} is mandatory");

            if (configurationFactory == null)
                throw new ArgumentException($"{nameof(configurationFactory)} is mandatory");

            if (tokenHandler == null)
                throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");

            if (eventHubFactory == null)
                throw new ArgumentException($"{nameof(eventHubFactory)} is mandatory");

            if (loggerFactory == null)
                throw new ArgumentException($"{nameof(loggerFactory)} is mandatory");

            if (tenantServiceFactory == null)
                throw new ArgumentException($"{nameof(tenantServiceFactory)} is mandatory");

            FlagServiceFactory = activityLogServiceFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            LookupServiceFactory = lookupServiceFactory;
        }

        private IFlagServiceFactory FlagServiceFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }
        private ILookupClientFactory LookupServiceFactory { get; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName,null,"system",null);
                    var reader = new StaticTokenReader(token.Value);
                    var eventhub = EventHubFactory.Create(reader);
                    var lookupService = LookupServiceFactory.Create(reader);
                    var flagConfiguration = ConfigurationFactory.Create<FlagConfiguration>(Settings.FlagEvent, reader).Get();
                    if (flagConfiguration == null)
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    else
                    {
                        if(flagConfiguration.Entities != null){
                            logger.Info($"#{flagConfiguration.Entities.Keys.Count} entity(ies) found from configuration: {Settings.ServiceName}");
                            logger.Info("-------------------------------------------");

                            var uniqueEvents = flagConfiguration.Entities
                                .SelectMany(ent => ent.Value.Select(c => c.EventName))
                                .Distinct()
                                .ToList();

                            var lookupEntries = lookupService.GetLookupEntries("entityTypes");
                            var activityLogService = FlagServiceFactory.Create(reader, logger,TokenHandler);
                            uniqueEvents.ForEach(eventName =>
                            {
                                eventhub.On(eventName, activityLogService.ProcessEvent(flagConfiguration, lookupEntries));
                                logger.Info($"It was made subscription to EventHub with the Event: #{eventName}");
                            });

                            logger.Info("-------------------------------------------");
                        } else{
                            logger.Info($"No entity found from configuration of tenant #{tenant.Id}");
                        }
                    }
                    eventhub.StartAsync();
                });
                logger.Info("Flag listener started");
            }
            catch (WebSocketException ex)
            {
                logger.Error("Error while listening eventhub to process Flag", ex);
                Start();
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub to process Flag", ex);
                logger.Info("\nFlag is working yet and waiting new event\n");
            }
        }
    }
}
