﻿using LendFoundry.DataAttributes.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;
using LendFoundry.EventHub;

namespace LendFoundry.Flag
{
    public class FlagServiceFactory : IFlagServiceFactory
    {
        public FlagServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFlagServiceExtended Create(StaticTokenReader reader, ILogger logger,ITokenHandler handler=null)
        {
        
            var flagRepositoryFactory = Provider.GetService<IFlagRepositoryFactory>();
            var flagRepository = flagRepositoryFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventhub = eventHubFactory.Create(reader);

            var lookupClientFactory = Provider.GetService<ILookupClientFactory>();
            var lookupService = lookupClientFactory.Create(reader);

            var datatAttribute = Provider.GetService<IDataAttributesClientFactory>();
            var datatAttributeService = datatAttribute.Create(reader);


            return new FlagService(logger, eventhub, lookupService, datatAttributeService, flagRepository,handler,reader);
        }
    }
}