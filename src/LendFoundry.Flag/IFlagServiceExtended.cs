﻿using LendFoundry.EventHub;
using System;
using System.Collections.Generic;

namespace LendFoundry.Flag
{
    public interface IFlagServiceExtended : IFlagService
    {
        Action<EventInfo> ProcessEvent(FlagConfiguration flagConfiguration, Dictionary<string, string> lookupEntries);
    }
}