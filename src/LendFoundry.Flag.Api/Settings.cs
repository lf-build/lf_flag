﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Flag.Api
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Environment variables
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "flag";
    }
}