﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Flag.Api.Controllers {
    /// <summary>
    /// ApiController
    /// </summary>
    [Route ("/")]
    public class ApiController : ExtendedController {
        /// <summary>
        /// ApiController
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public ApiController (IFlagService service, ILogger logger) : base (logger) {
            Service = service;
        }
        private NoContentResult NoContentResult { get; } = new NoContentResult ();
        private IFlagService Service { get; }

        /// <summary>
        /// AddOrRemoveTags
        /// </summary>
        /// <param name="flagId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("/{flagId}/tags")]
        //[ProducesResponseType(204)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult AddOrRemoveTags (string flagId, [FromBody] FlagActionRequest request) {
            return Execute (() => { Service.AddOrRemoveTags (flagId, request); return NoContentResult; });
        }

        /// <summary>
        /// UpdateStatus
        /// </summary>
        /// <param name="flagId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPut ("/{flagId}/{status}/status")]
        //[ProducesResponseType(typeof(IFlag), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> UpdateStatus (string flagId, FlagStatus status) {
            return ExecuteAsync (async () => Ok (await Service.UpdateStatus (flagId, status)));
        }

        /// <summary>
        /// AddFlag
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("/{entityType}/{entityId}")]
        //[ProducesResponseType(typeof(IFlag), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> AddFlag (string entityType, string entityId, [FromBody] FlagRequest request) {
            return ExecuteAsync (async () => Ok (await Service.AddFlag (entityType, entityId, request)));
        }

        /// <summary>
        /// AddOrRemoveActions
        /// </summary>
        /// <param name="flagId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("/{flagId}/actions")]
        //[ProducesResponseType(204)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult AddOrRemoveActions (string flagId, [FromBody] FlagActionRequest request) {
            return Execute (() => { Service.AddOrRemoveActions (flagId, request); return NoContentResult; });
        }

        /// <summary>
        /// GetAllFlag
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet ("/{entityType}/{entityId}/flags")]
        //[ProducesResponseType(typeof(IEnumerable<IFlag>), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetAllFlag (string entityType, string entityId) {
            return Execute (() => Ok (Service.GetAllFlag (entityType, entityId)));
        }

        /// <summary>
        /// GetAllFlags By eventName
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        [HttpGet ("/{entityType}/{eventName}")]
        //[ProducesResponseType(typeof(IEnumerable<IFlag>), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetAllFlagsByEventName (string entityType, string eventName) {
            return Execute (() => Ok (Service.GetAllFlagsByEventName (entityType, eventName)));
        }

        /// <summary>
        /// GetAllFlag
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet ("/{entityType}/all")]
        //[ProducesResponseType(typeof(IEnumerable<IFlag>), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetAllFlagsByEntityType (string entityType) {
            return Execute (() => Ok (Service.GetAllFlagsByEntityType (entityType)));
        }

        /// <summary>
        /// GetAllFlag by Tag Name
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        [HttpGet ("{entityType}/{tagName}/all")]
        //[ProducesResponseType(typeof(IEnumerable<IFlag>), 200)]
        //[ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetAllFlagsByTag (string entityType,string tagName) {
            return Execute (() => Ok (Service.GetAllFlagsByTag (entityType,tagName)));
        }

        private IActionResult GetObjectResult<T> (T obj) {
#if DOTNET2
            return new OkObjectResult (obj);
#else
            return new HttpOkObjectResult (obj);
#endif
        }

    }
}