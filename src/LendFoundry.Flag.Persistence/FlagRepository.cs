﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace LendFoundry.Flag.Persistence {
    public class FlagRepository : MongoRepository<IFlag, Flag>, IFlagRepository {
        static FlagRepository () {
            BsonClassMap.RegisterClassMap<Flag> (map => {
                map.AutoMap ();
                var type = typeof (Flag);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.MapProperty (s => s.EventData).SetSerializer (new BsonJsonSerializer<object> ());
                map.MapProperty (s => s.Status).SetSerializer (new EnumSerializer<FlagStatus> (BsonType.String));
                map.SetIsRootClass (true);
            });
        }

        public FlagRepository (ITenantService tenantService, IMongoConfiguration configuration):
            base (tenantService, configuration, "flag") {

                CreateIndexIfNotExists
                    (
                        indexName: "entity-type-id",
                        index : Builders<IFlag>.IndexKeys.Ascending (i => i.TenantId).Ascending (i => i.EntityType).Ascending (i => i.EntityId)
                    );

                CreateIndexIfNotExists
                    (
                        indexName: "entity-type-id-tags",
                        index : Builders<IFlag>.IndexKeys.Ascending (i => i.TenantId).Ascending (i => i.EntityType).Ascending (i => i.EntityId).Ascending (i => i.FlagTags)
                    );
            }

        public IEnumerable<IFlag> GetAllFlags (string entityType, string entityId) {
            Expression<Func<IFlag, bool>> query = q =>
                q.EntityType.ToLower () == entityType.ToLower () &&
                q.EntityId == entityId;

            return Query.Where (query).OrderByDescending (i => i.FlagCreatedDate).ToList (); //added the sort order to ensure proper order for all hits
        }

        public void AddTag (string flagId, string tag) {
            var tenantId = TenantService.Current.Id;
            var tagRecord = Query.FirstOrDefault (x => x.Id == flagId && x.TenantId == tenantId);
            var tags = tagRecord?.FlagTags.ToList ();
            bool addTag = true;
            if (tags != null) {
                if (tags.Contains (tag)) {
                    addTag = false;
                }
            }

            if (addTag == true) {
                Collection.UpdateOne (s =>
                    s.Id == flagId &&
                    s.TenantId == tenantId,
                    new UpdateDefinitionBuilder<IFlag> ()
                    .AddToSet<string> (a => a.FlagTags, tag), new UpdateOptions () { IsUpsert = true });
            }
        }
        public void RemoveTag (string flagId, string tag) {
            var tenantId = TenantService.Current.Id;

            var tagRecord = Query.FirstOrDefault (x => x.Id == flagId && x.TenantId == tenantId);
            var tagInfo = tagRecord?.FlagTags?.FirstOrDefault (x => x == tag);

            if (tagInfo != null) {
                Collection.UpdateOne (s =>
                    s.Id == flagId &&
                    s.TenantId == tenantId,
                    new UpdateDefinitionBuilder<IFlag> ()
                    .Pull<string> (a => a.FlagTags, tagInfo), new UpdateOptions () { IsUpsert = true });
            }
        }

        public void AddAction (string flagId, string action) {
            var tenantId = TenantService.Current.Id;
            var actionRecord = Query.FirstOrDefault (x => x.Id == flagId && x.TenantId == tenantId);
            var actions = actionRecord?.Action.ToList ();
            bool addAction = true;
            if (actions != null) {
                if (actions.Contains (action)) {
                    addAction = false;
                }
            }

            if (addAction == true) {
                Collection.UpdateOne (s =>
                    s.Id == flagId &&
                    s.TenantId == tenantId,
                    new UpdateDefinitionBuilder<IFlag> ()
                    .AddToSet<string> (a => a.Action, action), new UpdateOptions () { IsUpsert = true });
            }
        }
        public void RemoveAction (string flagId, string action) {
            var tenantId = TenantService.Current.Id;

            var actionRecord = Query.FirstOrDefault (x => x.Id == flagId && x.TenantId == tenantId);
            var actionInfo = actionRecord?.Action?.FirstOrDefault (x => x == action);

            if (actionInfo != null) {
                Collection.UpdateOne (s =>
                    s.Id == flagId &&
                    s.TenantId == tenantId,
                    new UpdateDefinitionBuilder<IFlag> ()
                    .Pull<string> (a => a.Action, actionInfo), new UpdateOptions () { IsUpsert = true });
            }
        }

        public IEnumerable<IFlag> GetAllFlagsByEventName (string entityType, string eventName) {
            Expression<Func<IFlag, bool>> query = q =>
                q.EntityType.ToLower () == entityType.ToLower () &&
                q.EventName.ToLower () == eventName.ToLower ();

            return Query.Where (query).OrderByDescending (i => i.FlagCreatedDate).ToList (); //added the sort order to ensure proper order for all hits
        }

        public IEnumerable<IFlag> GetAllFlagsByEntityType (string entityType) {
            return Query.Where (q => q.EntityType.ToLower () == entityType.ToLower ()).OrderByDescending (i => i.FlagCreatedDate).ToList<IFlag> ();;
        }

        public IEnumerable<IFlag> GetAllFlagsByTag (string entityType, string tag) {
            return Query.Where (f => f.FlagTags.Contains (tag) && f.EntityType.ToLower () == entityType.ToLower ()).ToList<IFlag> ();
        }

    }
}