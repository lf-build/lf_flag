﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace LendFoundry.Flag.Persistence
{
    //NOTE: Added for backward compatibility 
    public class BsonJsonSerializer<T> : IBsonSerializer<T>
    {
        private JsonSerializerSettings Settings { get; } = new JsonSerializerSettings()
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            if (context.Reader.CurrentBsonType == MongoDB.Bson.BsonType.Document)
            {
                var deserialized = BsonDocumentSerializer.Instance.Deserialize(context);
                return BsonSerializer.Deserialize<T>(deserialized);
            }
            var jsonString = context.Reader.ReadString();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString, Settings);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, T value)
        {
            var valueAsJson = Newtonsoft.Json.JsonConvert.SerializeObject(value, Settings);
            context.Writer.WriteString(valueAsJson);
        }

        public Type ValueType => typeof(T);

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            var valueAsJson = Newtonsoft.Json.JsonConvert.SerializeObject(value, Settings);
            context.Writer.WriteString(valueAsJson);
        }

        object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            //NOTE: Added for backward compatibility 
            if (context.Reader.CurrentBsonType == MongoDB.Bson.BsonType.Document)
            {
                var deserialized = BsonDocumentSerializer.Instance.Deserialize(context);
                return BsonSerializer.Deserialize<T>(deserialized);
            }
            var jsonString = context.Reader.ReadString();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString, Settings);

        }
    }

}
