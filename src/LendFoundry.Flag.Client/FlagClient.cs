﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;

namespace LendFoundry.Flag.Client {
    public class FlagClient : IFlagService {
        public FlagClient (IServiceClient client) {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IEnumerable<IFlag> GetAllFlag (string entityType, string entityId) {
            var request = new RestRequest ("/{entityType}/{entityId}/flags", Method.GET);
            request.AddUrlSegment (nameof (entityType), entityType);
            request.AddUrlSegment (nameof (entityId), entityId);

            return Client.Execute<List<Flag>> (request);
        }

        public void AddOrRemoveTags (string flagId, FlagActionRequest tagRequest) {
            var request = new RestRequest ("/{flagId}/tags", Method.POST);
            request.AddUrlSegment (nameof (flagId), flagId);
            request.AddJsonBody (tagRequest);

            Client.Execute (request);
        }

        public async Task<IFlag> UpdateStatus (string flagId, FlagStatus status) {
            var request = new RestRequest ("/{flagId}/{status}/status", Method.PUT);
            request.AddUrlSegment (nameof (flagId), flagId);
            request.AddUrlSegment (nameof (status), status.ToString ());
            return await Client.ExecuteAsync<Flag> (request);
        }

        public async Task<IFlag> AddFlag (string entityType, string entityId, IFlagRequest request) {
            var flagrequest = new RestRequest ("/{entityType}/{entityId}", Method.POST);
            flagrequest.AddUrlSegment (nameof (entityType), entityType);
            flagrequest.AddUrlSegment (nameof (entityId), entityId);
            flagrequest.AddJsonBody (request);

            return await Client.ExecuteAsync<Flag> (flagrequest);
        }

        public void AddOrRemoveActions (string flagId, FlagActionRequest actionRequest) {
            var request = new RestRequest ("/{flagId}/actions", Method.POST);
            request.AddUrlSegment (nameof (flagId), flagId);
            request.AddJsonBody (actionRequest);
            Client.Execute (request);
        }

        public IEnumerable<IFlag> GetAllFlagsByEventName (string entityType, string eventName) {
            var request = new RestRequest ("/{entityType}/{eventName}/flags", Method.GET);
            request.AddUrlSegment (nameof (entityType), entityType);
            request.AddUrlSegment (nameof (eventName), eventName);
            return Client.Execute<List<Flag>> (request);
        }
        public IEnumerable<IFlag> GetAllFlagsByEntityType (string entityType) {
            var request = new RestRequest ("/{entityType}/all", Method.GET);
            request.AddUrlSegment (nameof (entityType), entityType);
            return Client.Execute<List<Flag>> (request);
        }

        public IEnumerable<IFlag> GetAllFlagsByTag (string entityType,string tagName) {
            var request = new RestRequest ("{entityType}/{tagName}/all", Method.GET);
            request.AddUrlSegment (nameof (entityType), entityType);
            request.AddUrlSegment (nameof (tagName), tagName);
            return Client.Execute<List<Flag>> (request);
        }

    }
}