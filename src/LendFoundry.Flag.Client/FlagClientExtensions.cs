﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

using System;

namespace LendFoundry.Flag.Client
{
    public static class FlagClientExtensions
    {
        public static IServiceCollection AddFlag(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IFlagClientFactory>(p => new FlagClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IFlagClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddFlag(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IFlagClientFactory>(p => new FlagClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IFlagClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddFlag(this IServiceCollection services)
        {
            services.AddSingleton<IFlagClientFactory>(p => new FlagClientFactory(p));
            services.AddSingleton(p => p.GetService<IFlagClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}