﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Flag.Client
{
    public interface IFlagClientFactory
    {
        IFlagService Create(ITokenReader reader);
    }
}