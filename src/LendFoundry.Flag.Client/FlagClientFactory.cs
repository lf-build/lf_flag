﻿using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Client;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Flag.Client
{
    public class FlagClientFactory : IFlagClientFactory
    {
        public FlagClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

         public FlagClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }  

        private string Endpoint { get; set; }

        private int Port { get; set; }

        public IFlagService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("flag");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new FlagClient(client);
        }
    }
}