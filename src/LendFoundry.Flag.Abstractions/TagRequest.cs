﻿
namespace LendFoundry.Flag
{
    public class FlagActionRequest
    {
        public string[] FlagItems { get; set; }

        public TagAction Action { get; set; }
    }
}
