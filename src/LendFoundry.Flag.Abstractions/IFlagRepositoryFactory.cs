﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Flag
{
    public interface IFlagRepositoryFactory
    {
        IFlagRepository Create(ITokenReader reader);
    }
}