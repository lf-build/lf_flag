﻿namespace LendFoundry.Flag
{
    public enum FlagStatus
    {
        Active,
        Acknowledge,
        Dismiss
    }
}