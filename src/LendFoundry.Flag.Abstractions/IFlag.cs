﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Flag
{
    public interface IFlag : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string ProductId { get; set; }
        string[] FlagTags { get; set; }
        FlagDetail FlagDetail { get; set; }
        string EventName { get; set; }
        object EventData { get; set; }
        string[] Action { get; set; }
        DateTimeOffset FlagCreatedDate { get; set; }
        string Description { get; set; }
        FlagStatus Status { get; set; }
        string UserName { get; set; }

    }
}