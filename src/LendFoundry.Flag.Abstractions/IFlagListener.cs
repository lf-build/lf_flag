﻿namespace LendFoundry.Flag
{
    public interface IFlagListener
    {
        void Start();
    }
}