﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Flag
{
    public interface IFlagRepository : IRepository<IFlag>
    {
        IEnumerable<IFlag> GetAllFlags(string entityType, string entityId);
        void AddTag(string flagId, string tag);
        void RemoveTag(string flagId, string tag);
        void AddAction(string flagId, string action);
        void RemoveAction(string flagId, string action);
        IEnumerable<IFlag> GetAllFlagsByEventName(string entityType, string eventName);
        IEnumerable<IFlag> GetAllFlagsByEntityType(string entityType);
        IEnumerable<IFlag> GetAllFlagsByTag(string entityType,string tag);

    }
}