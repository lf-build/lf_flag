﻿using System.Collections.Generic;

namespace LendFoundry.Flag
{
    public interface IFlagRequest
    {
         string[] FlagTags { get; set; }
         FlagDetail FlagDetail { get; set; }
         object EventData { get; set; }
         string[] Action { get; set; }
         string Description { get; set; }
         string EventName { get; set; }

    }
}