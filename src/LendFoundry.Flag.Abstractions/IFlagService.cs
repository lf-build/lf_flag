﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Flag
{
    public interface IFlagService
    {      
        IEnumerable<IFlag> GetAllFlag(string entityType, string entityId);
        void AddOrRemoveTags(string flagId, FlagActionRequest tagRequest);
        Task<IFlag> UpdateStatus(string flagId, FlagStatus status);
        Task<IFlag> AddFlag(string entityType, string entityId, IFlagRequest request);
        void AddOrRemoveActions(string flagId, FlagActionRequest actionRequest);
        IEnumerable<IFlag> GetAllFlagsByEventName(string entityType, string eventName);
        IEnumerable<IFlag> GetAllFlagsByEntityType(string entityType);
        IEnumerable<IFlag> GetAllFlagsByTag(string entityType,string tag);

    }
}