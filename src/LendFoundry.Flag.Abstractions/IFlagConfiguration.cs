﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Flag
{
    public interface IFlagConfiguration : IDependencyConfiguration
    {
        Dictionary<string, IEnumerable<FlagEventConfiguration>> Entities { get; set; }

        string LookupEntityNameKey { get; set; }
        string ConnectionString { get; set; }
    }
}