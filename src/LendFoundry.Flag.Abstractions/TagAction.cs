﻿namespace LendFoundry.Flag
{
    public enum TagAction
    {
        Add,
        Remove
    }
}