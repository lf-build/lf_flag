﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Flag
{
    public class Flag : Aggregate, IFlag
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string ProductId { get; set; }
        public string[] FlagTags { get; set; }
        public FlagDetail FlagDetail { get; set; }
        public object EventData { get; set; }
        public string[] Action { get; set; }
        public DateTimeOffset FlagCreatedDate { get; set; }
        public string Description { get; set; }
        public FlagStatus Status { get; set; }
        public string EventName { get; set; }
        public string UserName { get; set; }
    }
}
