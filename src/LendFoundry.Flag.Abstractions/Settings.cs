﻿using System;

namespace LendFoundry.Flag
{
    public class Settings
    {
        private const string Prefix = "FLAG";
        public static string FlagEvent { get; } = "flag";
        public static string FlagAddedEventName => "FlagAdded";
        public static string ServiceName => Environment.GetEnvironmentVariable($"{Prefix}_TOKEN_ISSUER") ?? "flag";

    }
}