﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Flag
{
    public class FlagConfiguration : IFlagConfiguration , IDependencyConfiguration
    {
        public Dictionary<string, IEnumerable<FlagEventConfiguration>> Entities { get; set; }

         public string LookupEntityNameKey { get; set; } = "entityTypes";
         public Dictionary<string, string> Dependencies { get; set; }
         public string Database { get; set; }
         public string ConnectionString { get; set; }
    }
}