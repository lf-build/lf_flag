﻿using System.Collections.Generic;

namespace LendFoundry.Flag
{
    public class FlagEventConfiguration
    {
        public string EventName { get; set; }
        public string EntityId { get; set; }
        public string ProductId { get; set; }
        public FlagDetail FlagDetail { get; set; }
        public string[] Action { get; set; }
        public string[] Tags { get; set; }
        public string Description { get; set; }
    }
}