﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Flag
{
    public class FlagRequest : IFlagRequest
    {
        public string[] FlagTags { get; set; }
        public FlagDetail FlagDetail { get; set; }
        public object EventData { get; set; }
        public string[] Action { get; set; }
        public string Description { get; set; }
        public string EventName { get; set; }
         

    }
}
